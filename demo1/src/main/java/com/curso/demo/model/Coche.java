package com.curso.demo.model;

import org.springframework.beans.factory.annotation.Value;

import java.util.TreeSet;
import javax.persistence.*;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;


@Entity
public class Coche {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name = "ID",sequenceName = "IDC",initialValue = 1, allocationSize = 50)
    private Integer cocheId;
    @NotNull(message = "la matrícula no puede ser null")
    private String matricula;
    @NotNull(message = "el tipo de coche no puede ser null")
    @Enumerated(EnumType.STRING)
    private TipoCoche tipoCoche;
    @NotNull(message = "la marca no puede ser null")
    private String marca;
    @NotNull(message = "el modelo no puede ser null")
    private String modelo;
    @Min(value = 1)
    private int precioCompraVenta;
    @NotNull(message = "el número de exposición no puede ser null")
    private int numeroExposicion;
    @Value("${estadoCoche.ENSTOCK}")
    @Enumerated(EnumType.STRING)
    private EstadoCoche estadoCoche=EstadoCoche.ENSTOCK;
    @Enumerated(EnumType.STRING)
    private TipoReparacion tipoReparacion;


    public Coche(Integer cocheId,TipoCoche tipoCoche, String marca, String matricula, String modelo, int precioCompraVenta, int numeroExposicion,EstadoCoche estadoCoche) throws Exception {
        this.tipoCoche = tipoCoche;
        this.matricula = matricula;
        this.marca = marca;
        this.modelo = modelo;
        this.cocheId = cocheId;
        this.precioCompraVenta = precioCompraVenta;
        this.numeroExposicion = numeroExposicion;
        this.estadoCoche = estadoCoche;
    }

    public TipoReparacion getTipoReparacion() {
        return tipoReparacion;
    }

    public void setTipoReparacion(TipoReparacion tipoReparacion) {
        this.tipoReparacion = tipoReparacion;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public Coche() {
    }

    public void setCocheId(Integer cocheId) {
        this.cocheId = cocheId;
    }

    public void setPrecioCompraVenta(int precioCompraVenta) {
        this.precioCompraVenta = precioCompraVenta;
    }

    public TipoCoche getTipoCoche() {
        return tipoCoche;
    }

    public void setTipoCoche(TipoCoche tipoCoche) {
        this.tipoCoche = tipoCoche;
    }


    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public Integer getCocheId() {
        return cocheId;
    }

    public void setMatricula(Integer cocheId) {
        this.cocheId = cocheId;
    }

    public int getPrecioCompraVenta() {
        return precioCompraVenta;
    }


    public int getNumeroExposicion() {
        return numeroExposicion;
    }

    public void setNumeroExposicion(int numeroExposicion) {
        this.numeroExposicion = numeroExposicion;
    }

    public EstadoCoche getEstadoCoche() {
        return estadoCoche;
    }

    public void setEstadoCoche(EstadoCoche estadoCoche) {
        this.estadoCoche = estadoCoche;
    }

}

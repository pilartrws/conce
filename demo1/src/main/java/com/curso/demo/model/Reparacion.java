package com.curso.demo.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
public class Reparacion implements Comparable<Reparacion> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name = "ID", initialValue = 1, allocationSize = 100)
    private Integer reparacionId;
    private int numeroParte;

    private TipoReparacion tipoReparacion;
    @DateTimeFormat(pattern="MM/dd/yyyy")
    private long fechaReparacion;



    public Reparacion(int numeroParte,Integer reparacionId, TipoReparacion reparacion,long fechaReparacion) {
this.numeroParte=numeroParte;
        this.reparacionId = reparacionId;
        this.tipoReparacion = reparacion;

        this.fechaReparacion = fechaReparacion;
        Date creacion = new Date(System.currentTimeMillis() + Math.round(Math.random() * 1000));
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        this.setFechaReparacion(Long.parseLong(dateFormat.format(creacion)));
    }

    public Integer getReparacionId() {
        return reparacionId;
    }

    public int getNumeroParte() {
        return numeroParte;
    }

    public void setNumeroParte(int numeroParte) {
        this.numeroParte = numeroParte;
    }

    public void setReparacionId(Integer reparacionId) {
        this.reparacionId = reparacionId;
    }

    public TipoReparacion getTipoReparacion() {
        return tipoReparacion;
    }

    public void setTipoReparacion(TipoReparacion tipoReparacion) {
        this.tipoReparacion = tipoReparacion;
    }


    public long getFechaReparacion() {
        return fechaReparacion;
    }

    public Reparacion() {
        this.fechaReparacion = fechaReparacion;
        Date creacion = new Date(System.currentTimeMillis() + Math.round(Math.random() * 1000));
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        this.setFechaReparacion(Long.parseLong(dateFormat.format(creacion)));

    }


    public void setFechaReparacion(long fechaReparacion) {
        this.fechaReparacion = fechaReparacion;
    }

    @Override
    public int compareTo(Reparacion o) {
        if (this.fechaReparacion < o.fechaReparacion) return 1;
        if (this.fechaReparacion > o.fechaReparacion) return -1;
        return 0;
    }


}





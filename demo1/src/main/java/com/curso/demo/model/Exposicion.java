package com.curso.demo.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
public class Exposicion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name = "expoId", initialValue = 1, allocationSize = 100)
    private Integer expoId;
    @NotNull(message = "el número de exposición no puede estar vacío")
    private int numeroExposicion;
    @NotNull(message = "el campo de dirección no puede estar vacío")
    private String direccion;
    @NotNull(message = "el campo del número de teléfono no puede estar vacío")
    private int telefono;


    public Exposicion(Integer expoId, int numeroExposicion, String direccion, int telefono) {
        this.expoId = expoId;
        this.numeroExposicion = numeroExposicion;
        this.direccion = direccion;
        this.telefono = telefono;

    }

    public Exposicion() {
    }

    public Integer getExpoId() {
        return expoId;
    }

    public void setExpoId(Integer expoId) {
        this.expoId = expoId;
    }

    public int getNumeroExposicion() {
        return numeroExposicion;
    }

    public void setNumeroExposicion(int numeroExposicion) {
        this.numeroExposicion = numeroExposicion;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }
}

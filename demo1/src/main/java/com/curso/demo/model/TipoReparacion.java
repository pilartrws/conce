package com.curso.demo.model;

public enum TipoReparacion {

    MECANICA,
    ELECTRICA,
    CHAPA,
    REVISION,
}


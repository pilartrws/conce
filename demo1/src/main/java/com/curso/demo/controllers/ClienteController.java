package com.curso.demo.controllers;

import com.curso.demo.model.Cliente;
import com.curso.demo.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ClienteController {

    @Autowired
    private ClienteRepository clienteRepository;

    @RequestMapping("/greeting")
    public String greeting() {
        clienteRepository.save(new Cliente(34, "juan", "lalala", "38133174W"));
        clienteRepository.findById(34);
        return "Enhorabuena, has llegado al final de internet.";
    }


    @GetMapping("/cliente/{id}")
    public String cliente (@PathVariable Integer id){
        return "cliente "+id;
    }

    
}
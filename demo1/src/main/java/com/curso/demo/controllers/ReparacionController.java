package com.curso.demo.controllers;

import com.curso.demo.model.Coche;
import com.curso.demo.model.Reparacion;
import com.curso.demo.repositories.PartesReparacionRepository;
import com.curso.demo.repositories.CocheRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

//excepcion valores nulll
//metodos personalizados jpa
// exception handler

@RestController

public class ReparacionController {
    @Autowired
    private CocheRepository cocheRepository;
    @Autowired
    PartesReparacionRepository partesReparacionRepository;


    @GetMapping("reparaciones/{cocheId}")//funciona
    public List buscarCoche(@PathVariable Integer cocheId) throws Exception {

        Optional<Coche> coches = cocheRepository.findById(cocheId);
        if (!coches.isPresent()) {
            throw new Exception("el coche con id" + cocheId + "no existe");
        }
        List<Reparacion> partes = partesReparacionRepository.findByReparacionId(cocheId);
        if (partes.isEmpty()) {
            throw new Exception("No hay reparaciones que mostrar");
        }
        return partes;

    }


    @GetMapping("/partesReparacion")
    public List coches() throws Exception {
        List<Reparacion> partes = partesReparacionRepository.findAll();
        if (partes.isEmpty()) {
            throw new Exception("no hay partes de reparación");
        } else {
            return partes;
        }
    }


    @DeleteMapping("/borrarPartes/{cocheId}")//funciona
    public void delecteCoche(@PathVariable Integer cocheId) throws Exception {
        Optional<List<Reparacion>> partes = partesReparacionRepository.findByNumeroParte(cocheId);
        if (partes.isPresent()) {
            partesReparacionRepository.deleteById(cocheId);
        } else {
            throw new Exception("No hay partes registrados para este coche");
        }

    }


    @DeleteMapping("/borrarParte/{parteId}")//funciona
    public void delecte(@PathVariable Integer parteId) throws Exception {
        Optional<Reparacion> partes = partesReparacionRepository.findById(parteId);
        if (partes.isPresent()) {
            partesReparacionRepository.deleteById(parteId);
        } else {
            throw new Exception("No hay partes registrados para este coche");
        }
    }

    @PutMapping("/updateParte")
    public Reparacion updateIt(@RequestBody Reparacion reparacion) throws Exception {


        Optional<Reparacion> partes = partesReparacionRepository.findById(reparacion.getReparacionId());
        if (!partes.isPresent()) {
            throw new Exception("la exposición indicada no existe");
        }


        partesReparacionRepository.save(reparacion);


        return reparacion;
    }


}






package com.curso.demo.repositories;

import com.curso.demo.model.Cliente;
import com.curso.demo.model.Reparacion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface PartesReparacionRepository extends JpaRepository< Reparacion,Integer> {

   List<Reparacion> findByReparacionId(Integer reparacionId);
   Optional<List<Reparacion>> findByNumeroParte(Integer numeroParte);
}

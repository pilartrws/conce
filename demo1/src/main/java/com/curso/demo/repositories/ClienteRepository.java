package com.curso.demo.repositories;

import com.curso.demo.model.Cliente;
import com.curso.demo.model.Coche;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Integer> {
    Optional<List<Cliente>> findById(int id);
}

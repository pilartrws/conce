package com.curso.demo.repositories;

import com.curso.demo.model.Cliente;
import com.curso.demo.model.Coche;
import com.curso.demo.model.Exposicion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface ExposicionRepository extends JpaRepository<Exposicion, Integer> {

    Optional<Exposicion> findById(Integer expoId);

    Optional<Exposicion>findByNumeroExposicion(int numeroExposicion);


    void deleteByNumeroExposicion(int numeroExposicion);
}
